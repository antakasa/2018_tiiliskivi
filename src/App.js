import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { pointCalculations } from "./helpers.js";
import Data from "./data/data.js";
import QuizContainer from "./components/quizContainer/quizContainer.js";
import Some from "./components/resultWidget/someShare";
let mockDescriptions = {
  uupunut: "uupunut des",
  stressaantunut: "stressaantunut desc"
};

let choiceData = [];
let characterData = [];

for (var key in Data.choices) {
  if (Data.choices.hasOwnProperty(key)) {
    choiceData.push(Data.choices[key]);
  }
}

for (var key in Data.characters) {
  if (Data.characters.hasOwnProperty(key)) {
    characterData.push(Data.characters[key]);
  }
}

console.log(characterData);

class App extends Component {
  render() {
    return (
      <div className="App">
        <QuizContainer
          questionData={Data.questions}
          choiceData={choiceData}
          characterData={characterData}
          descriptions={Data.descriptions}
        />
      </div>
    );
  }
}

export default App;
