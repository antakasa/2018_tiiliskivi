let data = {
  questions: [
    "Tarvitsetko tilaa ympärillesi vai viihdytkö paremmin joukossa?",
    "Sinä ja pohjoisen neljä vuodenaikaa – jos pitää valita vain yksi?",
    "Vuorossa intiimit yksityiskohdat – kertoisitko suhteestasi seksiin?",
    "Suhteesi vaatteisiin ja pukeutumiseen?",
    "Mikä kuvaa parhaiten elämäntyyliäsi?",
    "Kun metsä ei ole vaihtoehto, missä viihdyt parhaiten – mikä kuvaa sielunmaisemaasi?",
    "Herkkähipiäinen... vai kovempi kuori? Klikkaa kuvaa, joka paremmin luonnehtii sinua!",
    "Klikkaa kuvaa, jonka sävy miellyttää sinua enemmän:"
  ],
  choices: {
    firstQuestion: [
      {
        teksti: "Viihdyn hyvin itsekseni."
      },
      {
        teksti: "Sekä että – aivan tilanteesta riippuen."
      },
      {
        teksti: "Väkijoukossa olen kuin kotonani – suorastaan nautin!"
      }
    ],
    secondQuestion: [
      {
        teksti: "Pidän eniten kesästä!"
      },
      {
        teksti: "Pidän eniten keväästä!"
      },
      {
        teksti: "Kaikki vuodenajat ovat mieluisia."
      },
      {
        teksti: "Pidän eniten syksystä."
      },
      {
        teksti: "Pidän eniten talvesta!"
      }
    ],
    thirdQuestion: [
      {
        teksti: "Olen hyvin nopeasti syttyvä."
      },
      {
        teksti: "Välttämätön paha, suvunjatkamista, siinä se!"
      },
      {
        teksti: "Mottoni on: Kohtuus kaikessa!"
      }
    ],
    fourthQuestion: [
      {
        teksti: "Vaatteet peittävät. Se on tärkeintä."
      },
      {
        teksti:
          "Omistamani vaatteet ovat rakkaita, käytän niitä kunnes ne lähes hajoavat!"
      },
      {
        teksti: "Kulutan aika paljon rahaa pukeutumiseen!"
      },
      {
        teksti: "Kierrätän paljon vaatteita."
      }
    ],
    fifthQuestion: [
      {
        teksti: "Live fast, die young."
      },
      {
        teksti: "Eteenpäin – hitaasti ja arvokkaasti."
      },
      {
        teksti: "Antaa sataa vaan – minä en vähästä hätkähdä!"
      },
      {
        teksti: "Vahvuuteni on yhteisöllisyydessä."
      },

      {
        teksti: "Bling bling! Bling bling!"
      }
    ],
    sixthQuestion: [
      {
        teksti: "Alavat, rehevät maat ja mannut."
      },
      {
        teksti: "Karut kalliot ja kukkulat."
      },
      {
        teksti: "Hämärän hyssy."
      },
      {
        teksti: "Heilimöivä niitty."
      },
      {
        teksti: "Rakennettu kulttuuriympäristö."
      }
    ],
    seventhQuestion: [
      {
        src:
          "https://lusi-dataviz.ylestatic.fi/2018_puunheimo_testi/imgs/koivu.jpg"
      },
      {
        src:
          "https://lusi-dataviz.ylestatic.fi/2018_puunheimo_testi/imgs/kaarna.jpg"
      }
    ],

    eighthQuestion: [
      {
        src:
          "https://lusi-dataviz.ylestatic.fi/2018_puunheimo_testi/imgs/vaalea.jpg"
      },
      {
        src:
          "https://lusi-dataviz.ylestatic.fi/2018_puunheimo_testi/imgs/tumma.jpg"
      }
    ]
  },
  descriptions: {
    haapa: {
      text: [
        {
          type: "text",
          value:
            "Kuulut ystävällisten haapojen heimoon – olet rakastava Äiti Amma!"
        },
        {
          type: "text",
          value:
            "Haapa on metsiemme tärkein puu: siinä elää tuhat eläin- ja kasvilajia – enemmän kuin millään muulla puulla Suomessa. Sammalet, käävät, linnut, näädät, perhoset, liito-oravat ja kovakuoriaiset – kaikki ne viihtyvät haavan huomassa."
        },
        {
          type: "text",
          value:
            "Haapa on todellinen elämän puu, metsiemme Äiti Amma, joka ottaa joka ikisen syleilyynsä."
        },
        {
          type: "text",
          value:
            "Haavan erityisvahvuus on sen juurissa: vaikka runko lahoaisi, haavan juuri selviää maaperässä pitkään ja nostattaa ylös uusia taimia. Lempeys on myös haapojen heimon heikkous: nuorena haavan taimet päätyvät hirvien suihin, koska ne maistuvat niin hyvälle."
        },
        {
          type: "text",
          value:
            "Haapa on hyvä kloonaamaan itseään – se voi siis lisääntyä ilman seksiä! Se yhteyttää lehtien lisäksi myös rungollaan."
        },
        {
          type: "text",
          value:
            "Lempeä haapa ei valtaa metsiämme kerralla tai kokonaan: kun muut lajit valtaavat metsää, haapa vetäytyy maan alle ja tekee comebackin, kun kilpailijoista on aika jättänyt."
        },
        {
          type: "text",
          value:
            "Haapojen heimoon kuuluvana sinäkin osaat olla todella ystävällinen – mutta muista, että kiltteydelläkin on rajansa!"
        }
      ]
    },
    koivu: {
      text: [
        {
          type: "text",
          value:
            "Kuulut koivujen rokkaavaan heimoon – olet metsiemme Amy Winehouse!"
        },
        {
          type: "text",
          value:
            "Koivujen heimo on metsiemme täysillä vetävä rokkibändi. Koivu viihtyy valokeilassa ja nauttii paljosta valosta! Koivut ottavat tarvitsemansa tilan haltuun – tyhjää alaa ne ovat valtaamassa ensimmäisten kasvien joukossa. Koivu-rokkarien managerina toimii myös ihminen istuttamalla taimia ja pitämällä huolen siitä, että hakkuuaukeita on tarjolla."
        },
        {
          type: "text",
          value:
            "Koivu on Suomen kolmanneksi yleisin puu – niitä kasvaa ihan joka kolkassa Utsjoelta Utöseen."
        },
        {
          type: "text",
          value:
            "Koivut kasvavat ja lisääntyvät nopeasti, mutta niissä on myös eroja: hieskoivu elää vain noin 50 vuotta – seitsenkymppinen hieskoivu on ihan ikäloppu! Rauduskoivu on toista maata: se voi todistaa elämän kiertokulkua jopa 200 vuotta."
        },
        {
          type: "text",
          value:
            "Koivu on yleinen puu, mutta sillä oma silmiinpistävä erikoisuutensa: aivan kuten rokkaritkin, pukeutuvat koivut näyttävästi – ja saavat valkoisine runkoineen muut puut näyttämään beigeiltä perusinsinööreiltä!"
        },
        {
          type: "text",
          value:
            "Koivu ja tuli ovat lyömätön yhdistelmä – kun on puhe sytykkeestä, ei ole tuohen voittanutta. Koivujen heimoon kuuluvana sinäkin osaat syttyä – ja sytyttää – rohkeasti!"
        }
      ]
    },
    kuusi: {
      text: [
        {
          type: "text",
          value:
            "Kuulut kuusien voitokkaaseen heimoon – olet metsiemme Julius Caesar!"
        },
        {
          type: "text",
          value:
            "Kuusi on hallanarka ja voisi yksinään helposti paleltua. Yhdessä kuuset ovat vahvoja!"
        },
        {
          type: "text",
          value:
            "Mahtavan armeijansa turvin tämä tehokas valtaaja kykenee muuttamaan kokonaisia metsiköitä. Kuusi toimii kuin valtava imperiumi, joka valtaa maan itselleen: Heikommat kilpailijat syrjäytetään. Ei armoa!"
        },
        {
          type: "text",
          value:
            "Kuusiheimon valttina on varjonkestävyys. Kaikessa rauhassa ne kasvavat varjossa täyteen mittaansa, saartaen kilpailijansa. Kuusen neulaset ovat niin tiheässä, ettei metsän pohjaan päädy valoa: turhapa yrittää kilpailijoiden ponnistaa! Toisaalta kuusen helmassa on maja jokaiselle."
        },
        {
          type: "text",
          value:
            "Kuusipuut muokkaavat sinnikkäästi maaperää: pudonneet neulaset muuttavat maan happamaksi, kuuselle parhaaksi. Kuusi tulee, näkee ja voittaa kuin Caesar konsanaan! Kuusien heimoon kuuluvana sinustakin on ystävien kanssa hyvä olla ja tiedät, että joukossa on voimaa!"
        },
        {
          type: "text",
          value:
            "PS. Kuusella on hyviä siemenvuosia ehkä vain kymmenen vuoden välein! Siinä sitä onkin miettimistä."
        }
      ]
    },
    manty: {
      text: [
        {
          type: "text",
          value:
            "Kuulut mäntyjen itsenäiseen heimoon – olet metsiemme vahva Rambo!"
        },
        {
          type: "text",
          value:
            "Mänty on metsiemme yksinäinen soturi – aivan kuten Rambo: se kestää paljon enemmän kuin toiset puut."
        },
        {
          type: "text",
          value:
            "Valon kasvi mänty kunnioittaa toisten tilaa eikä tule iholle! Mänty on Suomen yleisin puu – sen erityisvahvuus on monipuolisuus: se pärjää missä tahansa. Luodot, tunturit, metsät, suot – kaikki karut olosuhteet ovat kuin luotuja Rambolle."
        },
        {
          type: "text",
          value:
            "Männyille kasvaa usein paalujuuri, joka pitää ne tukevasti maassa. Eipä juuri myrskyä, joka männyn kaataisi! Mänty kestää jopa metsäpalon. Kun muu metsä palaa, hiiltyy metsiemme Rambo vain pinnastaan, paksun kilpikaarnan toimiessa haarniskana."
        },
        {
          type: "text",
          value: "Mänty voi elää jopa 800-vuotiaaksi."
        },
        {
          type: "text",
          value:
            "Mänty ei tarvitse joukkovoimaa suojakseen, se pärjää yksin omine aseineen: männyllä on oma kemiallinen puolustuksensa, joka suojaa sitä sieniltä ja bakteereilta. Männyn erittämät hajut ovat terveellisiä – ne karistavat pois myös ihmisen tauteja. Kun vietämme aikaa mäntymetsässä, puolustuskyky paranee. Mäntyjen heimoon kuuluvana löydät sinäkin vahvuudet läheltä: sinusta itsestäsi."
        }
      ]
    },
    tammi: {
      text: [
        {
          type: "text",
          value:
            "Kuulut tammien arvokkaaseen heimoon – olet ystävällinen velho Gandalf!"
        },
        {
          type: "text",
          value:
            "Tammi, metsiemme vaikuttava hahmo, auttaa muuta elämää: tammen runko on monen eliön koti. Karussa Pohjolassa tammi elää levinneisyytensä äärirajoilla. Ja kuten Gandalf sauvoineen tarvitsee tammikin ympärilleen paljon tilaa."
        },
        {
          type: "text",
          value:
            "Tammipuut tunnetaan erityisesti velhomaisista kemiantaidoistaan: niillä on käytössään ikiomat tehokkaat myrkkynsä, ja Suomen puista juuri tammella onkin paras lahonkestävyys. Sinäkin – tammen sukulaisena – pistät kenties helposti ja osuvasti vastaan ryppyilijöille?!"
        },
        {
          type: "text",
          value:
            "Toisin kuin tervaleppä, joka pudottaa lehtensä vihreinä, ottaa tammi lehdistään aivan kaikki ainekset talteen tiputtaen ne vasta rupiruskeina. Tammien heimo on pitkäikäistä. Tuhat vuotta mainittu!"
        },
        {
          type: "text",
          value:
            "Suomen puista tammia on alle yksi prosentti. Tammien heimoon kuuluvana sinäkin arvostat tilaa ympärilläsi!"
        }
      ]
    },
    kataja: {
      text: [
        {
          type: "text",
          value:
            "Kuulut sitkeään katajaiseen heimoon - olet metsiemme karjakkomummo!"
        },
        {
          type: "text",
          value:
            "Katajan sietokyky on omaa luokkaansa: kataja kasvaa pohjoisimpana ja korkeimmalla – jopa tunturin laella. Katajaisen heimon strategia onkin sinnitellä siellä, missä muiden puiden kantti pettää. Mestarisopeutujana katajasta tavataan monenlaisia muotoja: tarvittaessa se on matala kuin matto – ja yhtä usein se kasvaa ylväänä kuin pylväs!"
        },
        {
          type: "text",
          value:
            "Hyvän sietokykynsä ansiosta kataja on koko maailman laajimmalle levinnyt puusuku, ja se kuuluu Suomessakin viiden yleisimmän kasvin joukkoon."
        },
        {
          type: "text",
          value:
            "Kataja ei juuri apua pyytele – kaiken kokeneena se on valmis selviytymään yksin säässä kuin säässä. Katajaiseen heimoon kuuluvana sinäkin olet olennaisimpaan keskittyvä sitkeä pärjääjä!"
        }
      ]
    },
    tervaleppa: {
      text: [
        {
          type: "text",
          value:
            "Kuulut tervaleppien yltäkylläisyydessä elävään heimoon – olet metsiemme Kim Kardashian!"
        },
        {
          type: "text",
          value:
            "Toisin kuin puut yleensä, tervaleppä ei kitsastele – sillä on varaa tiputtaa lehtensä vihreinä, arvokkaan lehtivihreän kera!"
        },
        {
          type: "text",
          value:
            "Julkkis Kim Kardashianin tapaan tervalepän varallisuuden taustalla ovat tuottoisat  kumppanuudet. Kun ihmisillä on puutetta rahasta, on puilla puutetta typestä. Tervalepällä on kuitenkin ihan oma rahasto, “frankia-pankki”: puun juurissa elävät frankia-bakteerit sitovat ilmasta typpeä runsain mitoin tervalepän käyttöön. Tervaleppä voi jakaa typpivarojaan naapuripuille puhtaasti hyväntekeväisyytenä!"
        },
        {
          type: "text",
          value:
            "Eikö tervalepällä ole sitten mitään ongelmia? No, otapa Kimiltä kännykkä ja lompakko pois, niin jo muuttuu ääni kellossa! Tervalepän heikkous on, että se on kovin riippuvainen sekä frankia-bakteereistaan että vedestä."
        },
        {
          type: "text",
          value:
            "Tervaleppien heimoon kuuluvana sinä voit olla iloinen antaja – ja toisaalta osaat ottaa avosylin myös muilta vastaan."
        }
      ]
    }
  },
  characters: {
    kuusi: {
      0: "2",
      1: "4",
      2: "0",
      3: "3",
      4: "3",
      5: "2",
      6: "0",
      7: "1",
      name: "kuusi",
      someUrl:
        "https://share.api.yle.fi/share/puunheimo/caesar.html?url=artikkeli/2018/08/31/testaa-minka-puun-heimoon-sina-kuulut",
      taivutusMuoto: "",
      weight_1: "-1",
      weight_2: "-1",
      weight_3: "-1"
    },
    manty: {
      0: "0",
      1: "4",
      2: "1",
      3: "1",
      4: "1",
      5: "1",
      6: "0",
      7: "1",
      someUrl:
        "https://share.api.yle.fi/share/puunheimo/rambo.html?url=artikkeli/2018/08/31/testaa-minka-puun-heimoon-sina-kuulut",
      name: "manty",
      taivutusMuoto: "",
      weight_1: "-1",
      weight_2: "-1",
      weight_3: "-1"
    },
    tammi: {
      0: "0",
      1: "0",
      2: "2",
      3: "1",
      4: "1",
      5: "4",
      6: "1",
      7: "0",
      name: "tammi",
      someUrl:
        "https://share.api.yle.fi/share/puunheimo/gandalf.html?url=artikkeli/2018/08/31/testaa-minka-puun-heimoon-sina-kuulut",
      taivutusMuoto: "",
      weight_1: "-1",
      weight_2: "-1",
      weight_3: "-1"
    },
    koivu: {
      0: "1",
      1: "1",
      2: "0",
      3: "2",
      4: "0",
      5: "3",
      6: "1",
      7: "0",
      someUrl:
        "https://share.api.yle.fi/share/puunheimo/amy.html?url=artikkeli/2018/08/31/testaa-minka-puun-heimoon-sina-kuulut",
      name: "koivu",
      taivutusMuoto: "",
      weight_1: "-1",
      weight_2: "-1",
      weight_3: "-1"
    },
    tervaleppa: {
      0: "1",
      1: "1",
      2: "0",
      3: "2",
      4: "4",
      5: "0",
      6: "1",
      7: "1",
      name: "tervaleppa",
      taivutusMuoto: "",
      someUrl:
        "https://share.api.yle.fi/share/puunheimo/kardashian.html?url=artikkeli/2018/08/31/testaa-minka-puun-heimoon-sina-kuulut",
      weight_1: 1,
      weight_2: "-1",
      weight_3: "-1"
    },
    kataja: {
      0: "1",
      1: "2",
      2: "1",
      3: "0",
      4: "2",
      5: "1",
      6: "0",
      7: "1",
      name: "kataja",
      someUrl:
        "https://share.api.yle.fi/share/puunheimo/karjakkomummo.html?url=artikkeli/2018/08/31/testaa-minka-puun-heimoon-sina-kuulut",
      taivutusMuoto: "",
      weight_1: "-1",
      weight_2: "-1",
      weight_3: "-1"
    },
    haapa: {
      0: "2",
      1: "3",
      2: "2",
      3: "3",
      4: "2",
      5: "0",
      6: "1",
      7: "1",
      someUrl:
        "https://share.api.yle.fi/share/puunheimo/amma.html?url=artikkeli/2018/08/31/testaa-minka-puun-heimoon-sina-kuulut",
      name: "haapa",
      taivutusMuoto: "",
      weight_1: 2,
      weight_2: 0,
      weight_3: "-1"
    }
  }
};
export default data;
