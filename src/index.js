import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import initReactFastclick from "react-fastclick";
initReactFastclick();

const objectToValuesPolyfill = function(object) {
  return Object.keys(object).map(function(key) {
    return object[key];
  });
};

Object.values = Object.values || objectToValuesPolyfill;

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
