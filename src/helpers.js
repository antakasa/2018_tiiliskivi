//@flow
type characterDataType = {
  name: string,
  img: string,
  motto: string,
  weight_1: number,
  weight_2: number,
  weight_3: number,
  "0": number,
  "1": number,
  "2": number,
  "3": number,
  "4": number,
  "5": number,
  desc: string
};

const calculatePercentage = (
  pointCalculations: Array<Array<number | Object>>
): Object => {
  type functionReturn = () => void;
  let pointsAggregate = 0;
  return {
    calculateTotal: (): number => {
      if (pointsAggregate > 0) return pointsAggregate;
      for (let i = 0; i < pointCalculations.length; i++) {
        if (typeof pointCalculations[i][1] === "number") {
          pointsAggregate += pointCalculations[i][1];
        } else {
          throw "error";
        }
      }
      return pointsAggregate;
    },
    givePercentage: (points: number): number => {
      if (pointsAggregate === 0) {
        throw "error";
      }
      return points / pointsAggregate * 100;
    },
    pointsAggregate: () => pointsAggregate
  };
};

const CalculatePointsForSingleCharacter = (
  characterData: characterDataType,
  questions: Array<string>,
  answers: Array<number>
) => {
  let sum = 0;
  for (let i = 0; i < questions.length; i++) {
    let number = !isNaN(parseInt(characterData[i]))
      ? parseInt(characterData[i])
      : null;
    if (typeof number === "number" && characterData[i] == answers[i]) {
      sum += 1;
      //additional half point if matches weight
      if (
        i == characterData.weight_1 ||
        i == characterData.weight_2 ||
        i == characterData.weight_3
      ) {
        sum += 1.5;
      }
    } else {
    }
  }
  return sum;
};

const calculatePointsForAllCharacters = (
  characterData: Array<characterDataType>,
  questions: Array<string>,
  answers: Array<number>,
  calculateSingle: (
    characterData: characterDataType,
    questions: Array<string>,
    answers: Array<number>
  ) => number
): Array<Array<any>> => {
  let collectedPointsAllCharacters = [];
  for (let i = 0; i < characterData.length; i++) {
    let points = calculateSingle(characterData[i], questions, answers);
    collectedPointsAllCharacters.push([characterData[i], points]);
  }
  return collectedPointsAllCharacters;
};

const sortCharactersByPoints = (
  collectedPointsAllCharacters: Array<Array<mixed>>
): Array<Array<mixed>> =>
  collectedPointsAllCharacters.sort((a, b) => {
    if (typeof b[1] === "number" && typeof a[1] === "number") {
      return b[1] - a[1];
    } else {
      throw "error";
    }
  });

const pointCalculations = (
  characterData: Array<characterDataType>,
  questions: Array<string>,
  answers: Array<number>
): Array<Array<mixed>> => {
  const collectedPoints = calculatePointsForAllCharacters(
    characterData,
    questions,
    answers,
    CalculatePointsForSingleCharacter
  );
  const pointsSorted = sortCharactersByPoints(collectedPoints);
  return pointsSorted;
};

export {
  pointCalculations,
  CalculatePointsForSingleCharacter,
  sortCharactersByPoints,
  calculatePointsForAllCharacters,
  calculatePercentage
};
