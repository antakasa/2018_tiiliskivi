
Esimerkkivastaus: Vastauksesi muistuttivat 60% innostunutta, 30% stressaantunutta ja 10% uupunutta opiskelijatyyppiä.

(Tämä teksti tulisi automaattisesti sen mukaan mitä testin tekijä on vastannut. Alle aukeaisi sen opiskelijatyypin kuvaus minkä mukaan vastaaja on eniten vastannut. Muista tyypeistä linkit, joiden kautta pääsee lukemaan niiden kuvaukset ilman että oma tulos katoaa.)


```js

let mockDescriptions = {
    innostunut: {
      img: "https://loremflickr.com/320/240",
      kuvaus: [
        {
          type: "text",
          value:
            "Innostunut opiskelija kokee opiskelun useimmiten mielenkiintoisena ja tärkeänä. Hänen katseensa on tulevaisuudessa eikä hän murehdi menneitä. Innostunut opiskelija pystyy selviytymään epäonnistumisista, eivätkä kouluasiat vaivaa häntä vapaa-ajalla."
        },
        {
          type: "text",
          value:
            "Innostuneessa opiskelijaryhmässä voidaan erotella kaksi ryhmää, joista toinen on todella innostuneet ja intohimoiset opiskelijat. Tällaisia opiskelijoita on kuitenkin todellisuudessa vähän. Innostuneiksi opiskelijoiksi katsotaan myös ne opiskelijat, jotka suhtautuvat kouluun pääasiassa positiivisesti. Opiskelun ei siis tarvitse olla jatkuvaa riemua, vaan päivään voi kuulua niin nousuja kuin laskujakin. Innostus yhteenkin oppiaineeseen voi olla riittävää, sillä se voi läikkyä muihin oppiaineisiin ja laajemminkin muuhun opiskeluun."
        }
      ],
      tips: [
        {
          type: "text",
          value:
            "Tässä muutama vinkki, miten voit tehdä opiskelustasi vielä mukavampaa:"
        },
        {
          type: "text",
          value:
            "1. Mieti aamulla, mikä on tämän päivän tavoitteesi tai mitä ainakin haluat saada tehtyä? Tutkimukset ovat osoittaneet, että opiskelijat, jotka asettavat tavoitteita opiskelupäivälle, saavuttavat tavoitteensa paremmin ja kokevat enemmän positiivisia tunteita opiskelupäivän aikana kuin ne, joiden opiskelua ohjaavat ulkoiset tekijät, kuten velvollisuuden tunne ja muiden odotukset."
        },
        {
          type: "text",
          value:
            "2. Etsi innostavia opiskelukokemuksia miettimällä, milloin innostut opiskelusta eniten? Millaisessa tilanteessa olet ja kenen kanssa? Entä minkä aiheen parissa silloin yleensä työskentelet? Hakeutumalla tietoisesti tällaisiin tilanteisiin voi lisätä positiivisia tuntemuksia."
        }
      ]
    }
} 

let mockData = [[{
name: "innostunut",
taivutusMuoto: "uupunutta"
}, 1], 
[{
name: "stressaantunut",
taivutusMuoto: "stressaantunutta"
}, 2],
[{
name: "nul",
taivutusMuoto: "tän ei pitäisi näkyä"
}, 0]]
;
<ResultWidget data={mockData} descriptions={mockDescriptions} />
```

