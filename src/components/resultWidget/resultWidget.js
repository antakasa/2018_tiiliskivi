//@flow
import React, { Component } from "react";
import { calculatePercentage } from "../../helpers.js";
import styled from "styled-components";
import mantyPic from "../../imgs/results/Rambo_vastaus.jpg";
import katajaPic from "../../imgs/results/Karjakkomummo_vastaus.jpg";
import koivuPic from "../../imgs/results/Amy_vastaus.jpg";
import haapaPic from "../../imgs/results/Amma_vastaus.jpg";
import kuusiPic from "../../imgs/results/Caesar_vastaus.jpg";
import tammiPic from "../../imgs/results/Gandalf_vastaus.jpg";
import tervaleppaPic from "../../imgs/results/Kim_vastaus.jpg";
import Some from "./someShare";
let kuvat = {
  haapa: haapaPic,
  kuusi: kuusiPic,
  koivu: koivuPic,
  tammi: tammiPic,
  manty: mantyPic,
  tervaleppa: tervaleppaPic,
  kataja: katajaPic
};

type dataContainerArray = Array<number | Object>;

type propsType = {
  data: Array<dataContainerArray>,
  descriptions: Object
};

type state = {
  selected: string | null
};

const colors = {
  wrongAnswerBackground: "#f7fd5d",
  wrongAnswerSymbol: "#18b5ca",
  rightAnswerBackground: "#e76627",
  rightAnswerSymbol: "#fff"
};

const Container = styled.div`
  padding: 3vh;
  border: 1px solid #dcdcdc;
  border-radius: 3px;
  text-align: left;
`;

const Descriptions = props => {
  let data = props.data[props.selected].text;
  const kuva = kuvat[props.selected];
  let i, text;
  return (
    <div>
      <DescriptionImg src={kuva} />
      {data.map(e => <p> {e.value}</p>)}
    </div>
  );
};

const DescriptionImg = styled.img`
  width: 100%;
`;

const ReadMoreLink = styled.a`
  font-weight: bold;
  text-decoration: underline;
`;

class ResultWidget extends Component<propsType, state> {
  percentages: Object;
  filteredData: Array<dataContainerArray>;

  constructor(props: propsType) {
    super(props);
    this.state = {
      selected: null
    };
    this.filteredData = [];
    this.percentages = undefined;
  }

  whichPunctuationMark = (i: number): string => {
    if (i + 1 === this.filteredData.length - 1) {
      return " ja ";
    } else if (i + 1 === this.filteredData.length) {
      return " ";
    } else {
      return ", ";
    }
  };

  componentDidMount() {
    const element = document.querySelector("#abi_app_results");
    let images = [];

    if (this.state.selected !== null) {
      element.scrollIntoView({ behavior: "smooth" });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== null) {
      this.percentages = calculatePercentage(nextProps.data);
      this.percentages.calculateTotal();
      this.filteredData = nextProps.data.filter(e => e[1] > 0);
      nextProps.data !== null && typeof nextProps.data[0][0] === "object"
        ? this.setState({
            someUrl: nextProps.data[0][0].someUrl,
            selected: nextProps.data[0][0].name
          })
        : null;
    }
  }
  render() {
    return (
      <Container id="abi_app_results">
        {this.props.data === null ? (
          <p>Näet tuloksesi, kun olet vastannut kaikkiin kysymyksiin.</p>
        ) : (
          <div>
            {this.state.selected !== null && (
              <React.Fragment>
                <Descriptions
                  data={this.props.descriptions}
                  selected={this.state.selected}
                />
                <Some url={this.state.someUrl} />
              </React.Fragment>
            )}
          </div>
        )}
      </Container>
    );
  }
}

export default ResultWidget;
