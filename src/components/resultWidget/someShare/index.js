import React from "react";
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterIcon,
  TwitterShareButton,
  WhatsappShareButton,
  WhatsappIcon
} from "react-share";

export default ({ url }) => (
  <div>
    <h1>Jaa tulos kavereillesi:</h1>
    <div
      style={{
        display: "flex",
        justifyContent: "space-around",
        width: "250px"
      }}
    >
      <FacebookShareButton url={url}>
        <FacebookIcon
          iconBgStyle={{ fill: "rgb(51, 113, 14)" }}
          size={64}
          round
        />
      </FacebookShareButton>
      <TwitterShareButton
        url={url}
        via={"yleluonto"}
        hashtags={["elävätpuut"]}
        title={"Testaa minkä puun heimoon sinä kuulut!"}
      >
        <TwitterIcon
          iconBgStyle={{ fill: "rgb(51, 113, 14)" }}
          size={64}
          round
        />
      </TwitterShareButton>
      <WhatsappShareButton url={url}>
        <WhatsappIcon
          iconBgStyle={{ fill: "rgb(51, 113, 14)" }}
          size={64}
          round
        />
      </WhatsappShareButton>
    </div>
  </div>
);
