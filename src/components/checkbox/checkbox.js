// @flow
import React from "react";
import styled from "styled-components";

type CheckBoxProps = {
  text: String,
  checked?: boolean,
  userAnsweredRight?: boolean,
  rightAnswer?: string,
  onClick?: () => void
};

const colors = {
  wrongAnswerBackground: "#f7fd5d",
  wrongAnswerSymbol: "#18b5ca",
  rightAnswerBackground: "black",
  rightAnswerSymbol: "#fff"
};

const DrawCheckMarkCheck = (text, rightAnswer, checked, userAnsweredRight) =>
  text === rightAnswer || checked || rightAnswer === "ABC";

const CustomCheckbox = (props: CheckBoxProps) => {
  const {
    text,
    someIsAnswered,
    rightAnswer,
    userAnsweredRight,
    checked,
    onClick
  } = props;
  // arguments passed for DrawCheckMarkCheck()
  const args = [text, rightAnswer, checked, userAnsweredRight];
  const StyledInput = styled.input`
    position: absolute;
    opacity: 0;
    cursor: pointer;
  `;
  const StyledLabel = styled.label`
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    user-select: none;
    text-align: left;
    color: ${checked ? colors.rightAnswerBackground : "black"};
    opacity: ${someIsAnswered && !checked ? 0.3 : 1};
    font-weight: ${checked ? "bold" : "normal"};
  `;

  const StyledSpan = styled.span`
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: ${DrawCheckMarkCheck(
      text,
      checked,
      rightAnswer,
      userAnsweredRight
    )
      ? `${colors.rightAnswerBackground}`
      : "#eee"};
    &:after {
      position: absolute;
      display: none;
      left: 10px;
      content: "";
      top: 5px;
      display: block;
      width: 7px;
      height: 10px;
      ${DrawCheckMarkCheck(...args)
        ? `border: solid ${colors.rightAnswerSymbol};
    border-width: 0 3px 3px 0;
	transform: rotate(40deg);`
        : ``};
    }
    ${StyledLabel}:hover & {
      background-color: ${props.text === props.rightAnswer ? `#ccc` : "#ccc"};
    }
    ${StyledInput}:checked ~ & {
      background-color: ${userAnsweredRight || checked
        ? "#33710e"
        : colors.wrongAnswerBackground};
      &:after {
        content: ${DrawCheckMarkCheck(...args) ? "''" : "'X'"};
        left: ${DrawCheckMarkCheck(...args) ? "9px" : "5px"};
        top: ${DrawCheckMarkCheck(...args) ? "5px" : "0px"};
        color: ${colors.wrongAnswerSymbol};
        font-weight: bold;
        display: block;
      }
    }
  `;

  return (
    <StyledLabel>
      {text}
      <StyledInput
        checked={checked ? "checked" : ""}
        onChange={() => {
          if (typeof onClick === "function") {
            onClick();
          }
        }}
        type="checkbox"
      />
      <StyledSpan />
    </StyledLabel>
  );
};

export default CustomCheckbox;
