//@flow
import React, { Component } from "react";
import QuestionContainer from "../questionContainer/questionContainer.js";
import styled from "styled-components";
type State = {
  answers: { [number]: number }
};

type props = {
  choiceData: Array<Array<string>>,
  allQuestionsAnsweredCallback: Object => void,
  questionData: Array<string>
};

const QuestionText = styled.p`
  text-align: left;
  font-weight: bold;
`;

const ContainerForSingleQuestion = styled.div`
  margin-bottom: 7vh;
`;

export default class QuestionList extends Component<props, State> {
  answers: Object;
  state = { answers: {} };

  clickHandler = (questionNumber: number, choiceNumber: number) => {
    this.answers = { ...this.answers, [questionNumber]: choiceNumber };
    if (
      Object.keys(this.answers).length === this.props.choiceData.length &&
      typeof this.props.allQuestionsAnsweredCallback === "function"
    )
      this.props.allQuestionsAnsweredCallback(this.answers);
  };

  render() {
    return (
      <div>
        {this.props.choiceData.map((e, questionNumber) => (
          <ContainerForSingleQuestion key={questionNumber}>
            <QuestionContainer
              questionText={this.props.questionData[questionNumber]}
              questionNumber={`${questionNumber + 1}/${
                this.props.choiceData.length
              }`}
              questionData={e}
              sendPickedChoiceToParent={(choiceNumber: number): void => {
                this.clickHandler(questionNumber, choiceNumber);
              }}
            />
          </ContainerForSingleQuestion>
        ))}
      </div>
    );
  }
}
