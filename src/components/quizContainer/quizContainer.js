//@flow
import React, { Component } from "react";
import QuestionContainer from "../questionContainer/questionContainer.js";
import styled from "styled-components";
import QuestionList from "../questionList/questionList.js";
import { pointCalculations } from "../../helpers.js";
import ResultWidget from "../resultWidget/resultWidget.js";
type characterDataType = {
  name: string,
  img: string,
  motto: string,
  weight_1: number,
  weight_2: number,
  weight_3: number,
  "0": number,
  "1": number,
  "2": number,
  "3": number,
  "4": number,
  "5": number,
  desc: string
};

type State = {
  finished: boolean,
  answers: Object,
  result: null | Object
};

type props = {
  questionData: Array<string>,
  choiceData: Array<Array<string>>,
  characterData: characterDataType,
  descriptions: Array<string>
};

//const ContainerForSingleQuestion = styled.div`
//margin-bottom: 7vh;
//`;

export default class QuizContainer extends Component<props, State> {
  answers: object;
  state = { finished: false, answers: {}, result: null };

  clickHandler = (questionNumber: number, choiceNumber: number) => {};

  quizFinished = (answers: Object) => {
    console.log(answers);
    if (answers !== this.answers) {
      this.answers = answers;
      this.setState({
        finished: true,
        result: this.resultCalculations(answers)
      });
    }
    //if (!this.state.finished) {
    //this.setState({ finished: true, answers: answers });
    //}
  };

  resultCalculations = (answers: Object) => {
    let answersArray = Object.values(answers);
    let result = pointCalculations(
      this.props.characterData,
      this.props.questionData,
      answersArray
    );
    return result;
  };

  componentDidUpdate() {}

  render() {
    return (
      <div>
        <QuestionList
          allQuestionsAnsweredCallback={(answers: Object) =>
            this.quizFinished(answers)
          }
          choiceData={this.props.choiceData}
          questionData={this.props.questionData}
        />
        <ResultWidget
          data={this.state.result}
          descriptions={this.props.descriptions}
        />
      </div>
    );
  }
}
