```js
let mockData = [
  [
"f", "fefe", "ASDD"  
],
  ["FADF", "FDA", "DACS" 
]
];
	
let characterMockData = [
  {
    name: "uupunut",
    img: "lisa.jpg",
    taivutusMuoto: "uupunutta",
    motto: "Faktoilla voittoon! Millainen lukija sinä olet? Tee testi!",
    weight_1: 5,
    weight_2: 1,
    weight_3: -1,
    "0": 0,
    "1": 1,
    "2": 2,
    "3": 3,
    "4": 3,
    "5": 3,
    desc:
      "Faktoilla voittoon! Sinä rentoudut Kekkosen elämäkerran, jogurtin valmistuksen, 1800-luvun arkkitehtuurien helmien ja molekyylibiologian parissa. Mitä sitä draamalla kun todellisuus on vielä ihmeellisempää!"
  },
  {
    name: "stressaantunutta",
    taivutusMuoto: "väsynyttä",
    img: "lisa.jpg",
    motto: "Faktoilla voittoon! Millainen lukija sinä olet? Tee testi!",
    weight_1: 5,
    weight_2: 1,
    weight_3: -1,
    "0": 2,
    "1": 1,
    "2": 3,
    "3": 3,
    "4": 3,
    "5": 3,
    desc:
      "Faktoilla voittoon! Sinä rentoudut Kekkosen elämäkerran, jogurtin valmistuksen, 1800-luvun arkkitehtuurien helmien ja molekyylibiologian parissa. Mitä sitä draamalla kun todellisuus on vielä ihmeellisempää!"
  }
];
let questionData = ["asdg", "asdg", "#asdg"];

let mockDescriptions = {
	"uupunut": "uupunut des",
   "stressaantunut": "stressaantunut desc"
}; 
<QuizContainer questionData={questionData} choiceData={mockData} characterData={characterMockData} descriptions={mockDescriptions} />
```

