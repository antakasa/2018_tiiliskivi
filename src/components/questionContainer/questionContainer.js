//@flow
import React, { Component } from "react";
import CustomCheckbox from "../checkbox/checkbox.js";
import styled from "styled-components";

const ChoiceImage = styled.img`
  width: calc(50% - 20px);
  margin: 10px;
  opacity: ${props => (props.someIsAnswered && !props.selected ? "0.3" : "1")};
`;

const QuestionText = styled.p`
  text-align: left;
  font-weight: bold;
  color: #33710e;
`;

type State = {
  selected: number
};

type props = {
  questionData: Array<String>,
  sendPickedChoiceToParent: (i: number) => void
};

export default class QuestionContainer extends Component<props, State> {
  state = { selected: -1 };

  clickHandler = (i: number) => {
    this.props.sendPickedChoiceToParent(i);
    this.setState({ selected: i });
  };

  componentDidUpdate() {}

  render() {
    const Container = styled.div``;

    return (
      <Container>
        <QuestionText>
          {this.props.questionNumber}
          {". "}
          {this.props.questionText}
        </QuestionText>
        {this.props.questionData.map((e, choiceNumber) => {
          if (e.src)
            return (
              <ChoiceImage
                onClick={() => this.clickHandler(choiceNumber)}
                src={e.src}
                someIsAnswered={this.state.selected > -1}
                selected={choiceNumber === this.state.selected}
              />
            );
          return (
            <CustomCheckbox
              someIsAnswered={this.state.selected > -1}
              key={choiceNumber}
              checked={choiceNumber === this.state.selected}
              text={e.teksti}
              onClick={() => this.clickHandler(choiceNumber)}
            />
          );
        })}
      </Container>
    );
  }
}
