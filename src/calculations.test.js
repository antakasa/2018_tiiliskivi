import React from "react";
import ReactDOM from "react-dom";
import {
  CalculatePointsForSingleCharacter,
  calculatePointsForAllCharacters,
  sortCharactersByPoints,
  calculatePercentage
} from "./helpers.js";

let questionsMock = ["1", "2", "3"];

let answersMock = [];

let characterMockData = [
  {
    name: "Asia-Pentti",
    img: "lisa.jpg",
    motto: "Faktoilla voittoon! Millainen lukija sinä olet? Tee testi!",
    weight_1: 5,
    weight_2: 1,
    weight_3: -1,
    "0": 2,
    "1": 1,
    "2": 3,
    "3": 3,
    "4": 3,
    "5": 3,
    desc:
      "Faktoilla voittoon! Sinä rentoudut Kekkosen elämäkerran, jogurtin valmistuksen, 1800-luvun arkkitehtuurien helmien ja molekyylibiologian parissa. Mitä sitä draamalla kun todellisuus on vielä ihmeellisempää!"
  },
  {
    name: "Asia-Pentti",
    img: "lisa.jpg",
    motto: "Faktoilla voittoon! Millainen lukija sinä olet? Tee testi!",
    weight_1: 5,
    weight_2: 1,
    weight_3: -1,
    "0": 2,
    "1": 1,
    "2": 3,
    "3": 3,
    "4": 3,
    "5": 3,
    desc:
      "Faktoilla voittoon! Sinä rentoudut Kekkosen elämäkerran, jogurtin valmistuksen, 1800-luvun arkkitehtuurien helmien ja molekyylibiologian parissa. Mitä sitä draamalla kun todellisuus on vielä ihmeellisempää!"
  }
];

it("all characters calculations works with mock data", () => {
  answersMock = [...answersMock, 2, 1, 3];
  let b = calculatePointsForAllCharacters(
    characterMockData,
    questionsMock,
    answersMock,
    CalculatePointsForSingleCharacter
  );
  let arr = [[characterMockData[0], 3]];
  expect(b[0]).toContain(3);
});

it("calculates percentage", () => {
  let result = sortCharactersByPoints([["toka", 1], ["eka", 2]]);
  let percentages = calculatePercentage(result);
  let total = percentages.calculateTotal();
  expect(total).toBe(3);
  expect(percentages.givePercentage(3)).toBe(100);
});

it("sorts in descending order", () => {
  let result = sortCharactersByPoints([["toka", 1], ["eka", 2]]);
  expect(result[0][1]).toBeGreaterThan(result[1][1]);
  console.log(result);
});

it("single character calculations works with mock data", () => {
  expect(
    CalculatePointsForSingleCharacter(
      characterMockData[0],
      questionsMock,
      answersMock
    )
  ).toBe(3);
});
